# husky-cloud

## 1、仓库说明

SpringCloud综合入门案例

## 2、分类文档

- [项目技术选型简介，架构图解说明](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/cloud/senior/S01、入门案例选型.md)
- [业务架构设计，系统分层管理](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/cloud/senior/S02、入门案例业务流.md)
- [数据库选型，业务数据规划设计](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/cloud/senior/S03、入门案例数据库.md)
- [中间件集成，公共服务封装](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/cloud/senior/S04、入门案例中间件.md)
- [SpringCloud基础组件应用设计](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/cloud/senior/S05、入门案例微服务组件.md)
- [通过业务、应用、技术，聊聊架构](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/cloud/senior/S06、入门案例总结.md)

## 3、仓库整合

| 仓库 | 描述 |
|:---|:---|
| [butte-java](https://gitee.com/cicadasmile/butte-java-note) |Java编程文档整理，基础、架构，大数据 |
| [butte-frame](https://gitee.com/cicadasmile/butte-frame-parent) |微服务组件，中间件，常用功能二次封装 |
| [butte-flyer](https://gitee.com/cicadasmile/butte-flyer-parent) |butte-frame二次浅封装，实践案例 |
| [butte-auto](https://gitee.com/cicadasmile/butte-auto-parent) |Jenkins+Docker+K8S实现自动化持续集成 |
| [java-base](https://gitee.com/cicadasmile/java-base-parent) | Jvm、Java基础、Web编程，JDK源码分析 |
| [model-struct](https://gitee.com/cicadasmile/model-arithmetic-parent) | 设计模式、数据结构、算法 |
| [data-manage](https://gitee.com/cicadasmile/data-manage-parent) | 架构设计，实践，数据管理、工具 |
| [spring-mvc](https://gitee.com/cicadasmile/spring-mvc-parent) | Spring+Mvc框架基础总结 |
| [spring-boot](https://gitee.com/cicadasmile/spring-boot-base) | SpringBoot2基础，应用、配置等 |
| [middle-ware](https://gitee.com/cicadasmile/middle-ware-parent) | SpringBoot2进阶，整合常用中间件 |
| [spring-cloud](https://gitee.com/cicadasmile/spring-cloud-base) | Spring+Ali微服务基础组件用法|
| [cloud-shard](https://gitee.com/cicadasmile/cloud-shard-jdbc) | SpringCloud实现分库分表实时扩容 |
| [husky-cloud](https://gitee.com/cicadasmile/husky-spring-cloud) | SpringCloud综合入门案例 |
| [big-data](https://gitee.com/cicadasmile/big-data-parent) | Hadoop框架，大数据组件，数据服务 |
| [mysql-base](https://gitee.com/cicadasmile/mysql-data-base) | MySQL数据库基础、进阶总结 |
| [linux-system](https://gitee.com/cicadasmile/linux-system-base) | Linux系统基础，环境搭建、配置 |
